<?php

/**
 * General configuration.
 */
function monkeylearn_config_form() {
  $form = array();

  $form['monkeylearn_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('API token'),
    '#default_value' => variable_get('monkeylearn_api_token', ''),
  );
  $form['monkeylearn_api_version'] = array(
    '#type' => 'select',
    '#title' => t('API Version'),
    '#options' => array(
      'v1' => t('Version 1'),
    ),
    '#default_value' => variable_get('monkeylearn_api_version', 'v1'),
  );

  return system_settings_form($form);
}
