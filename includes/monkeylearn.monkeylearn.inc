<?php

use GuzzleHttp\Client;

abstract class monkeyLearn {

  public $version = '';
  protected $guzzle_client = NULL;

   public function __construct() {
    $this->version = variable_get('monkeylearn_api_version', 'v1');

    $this->guzzle_client = new Client([
      'base_url' => array('https://api.monkeylearn.com', array()),
      'defaults' => [
          'headers' => ['Authorization' => 'token ' . variable_get('monkeylearn_api_token')],
      ]
  ]);
  }
}
